module gitlab.com/paddycarver/delete-gcp-test-projects

require (
	github.com/fatih/color v1.7.0 // indirect
	github.com/hashicorp/terraform v0.11.10
	github.com/mattn/go-colorable v0.0.9 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/mitchellh/go-homedir v1.0.0 // indirect
	golang.org/x/net v0.0.0-20181201002055-351d144fa1fc // indirect
	golang.org/x/oauth2 v0.0.0-20181203162652-d668ce993890
	golang.org/x/sync v0.0.0-20181108010431-42b317875d0f // indirect
	golang.org/x/sys v0.0.0-20181206074257-70b957f3b65e // indirect
	google.golang.org/api v0.0.0-20181206211257-1a5ef82f9af4
	google.golang.org/appengine v1.3.0 // indirect
	yall.in v0.0.1
)

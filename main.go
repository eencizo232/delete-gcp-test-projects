package main

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"github.com/hashicorp/terraform/helper/pathorcontents"
	"golang.org/x/oauth2/jwt"
	cloudresourcemanager "google.golang.org/api/cloudresourcemanager/v1"
	yall "yall.in"
	"yall.in/colour"
)

type accountFile struct {
	PrivateKeyId string `json:"private_key_id"`
	PrivateKey   string `json:"private_key"`
	ClientEmail  string `json:"client_email"`
	ClientId     string `json:"client_id"`
}

func main() {
	log := yall.New(colour.New(os.Stdout, yall.Debug))
	credentials := os.Getenv("GOOGLE_CREDENTIALS")
	contents, _, err := pathorcontents.Read(credentials)
	if err != nil {
		log.WithError(err).WithField("credentials", credentials).Error("Error reading credentials")
		os.Exit(1)
	}
	var account accountFile
	r := strings.NewReader(contents)
	dec := json.NewDecoder(r)
	err = dec.Decode(&account)
	if err != nil {
		log.WithError(err).Error("Error loading account file")
		os.Exit(1)
	}
	conf := jwt.Config{
		Email:      account.ClientEmail,
		PrivateKey: []byte(account.PrivateKey),
		Scopes: []string{
			"https://www.googleapis.com/auth/cloud-platform",
		},
		TokenURL: "https://accounts.google.com/o/oauth2/token",
	}

	client := conf.Client(context.Background())
	crm, err := cloudresourcemanager.New(client)
	if err != nil {
		log.WithError(err).Error("Error creating cloud resource manager client")
		os.Exit(1)
	}
	var pending, total int
	var projects []string
	err = crm.Projects.List().Pages(context.Background(), func(resp *cloudresourcemanager.ListProjectsResponse) error {
		for _, project := range resp.Projects {
			total++
			if project.LifecycleState != "ACTIVE" {
				pending++
				continue
			}
			if strings.ToLower(project.Name) != "terraform acceptance tests" {
				continue
			}
			if !strings.HasPrefix(project.ProjectId, "terraform-") {
				continue
			}
			projects = append(projects, project.ProjectId)
		}
		return nil
	})
	if err != nil {
		log.WithError(err).Error("Error listing projects")
		os.Exit(1)
	}
	fmt.Println(total, "projects total")
	fmt.Println(pending, "projects pending deletion")
	fmt.Println("Going to delete", len(projects), "projects:")
	for _, project := range projects {
		fmt.Println("\t" + project)
	}
	reader := bufio.NewReader(os.Stdin)
	fmt.Printf("Enter 'yes' to delete: ")
	resp, err := reader.ReadString('\n')
	if err != nil {
		log.WithError(err).Error("error reading input")
		os.Exit(1)
	}
	resp = strings.Replace(resp, "\n", "", -1)
	if resp != "yes" {
		log.WithField("response", resp).Error("'yes' not entered, aborting")
		os.Exit(1)
	}
	for _, project := range projects {
		_, err = crm.Projects.Delete(project).Do()
		l := log.WithField("project", project)
		if err != nil {
			l.WithError(err).Error("Error deleting project")
			os.Exit(1)
		}
		l.Info("deleted project")
	}
}
